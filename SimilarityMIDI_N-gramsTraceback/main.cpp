#include <string>
#include <vector>
#include <unistd.h>
#include <string.h>
#include <stdio.h>
#include <algorithm>
#include "midifile.h"
#include <dirent.h>
#include <iostream>
#include <fstream>
#include <math.h>

using namespace std;

#define MAX_SIZE 100000
 //MIDI FILES


void read_midi(string midifile, int *midi, int *midiSize)
{
  MidiFileEvent_t event;
  MidiFile_t md = MidiFile_load(midifile.c_str());
  event = MidiFile_getFirstEvent(md);

  *midiSize = 0;
  
  /* boucle principale */
  while(event)
    {
      if (MidiFileEvent_isVoiceEvent(event))
	{
	  if (MidiFileEvent_isNoteStartEvent(event))
	    {
	      /* note start */

	      // Channel
	      //MidiFileNoteStartEvent_getChannel(event)
	      // Time
	      // MidiFileEvent_getTick(event)
	      // Pitch
	      midi[*midiSize] = MidiFileNoteStartEvent_getNote(event);
	      *midiSize = *midiSize+1;
	      
	      // Duration
	      // MidiFileEvent_getTick(MidiFileNoteStartEvent_getNoteEndEvent(event))-MidiFileEvent_getTick(event)
	      
	      // Attention channel 10 !!
	    }
	}
      event = MidiFileEvent_getNextEventInFile(event);
    }
}


static int file_select(const struct dirent   *entry) 
{

  if (strstr(entry->d_name, ".mid") != NULL) 
    {
      if (strlen(strstr(entry->d_name, ".mid")) == 4)
	return (1);
      else
	return (0);
    }
  else
    return (0);
}

float Ngrams (int n , int *midi1, int midiSize1, int *midi2, int midiSize2) {
  vector<vector<int>> notes;
  float cpt = 0;

  for(int i = 0 ; i < midiSize1-n+1 ; i++) {
    for(int j = 0; j < midiSize2-n+1 ; j++) {
      vector<int> tmp;
      bool same = true;
      bool repeated = false;
      for(int k = 0 ; k < n; k++) {
        // check if we have the same passage of N
        // insert passage of N in tmp
        tmp.push_back(*(midi1+i+k));
        if(*(midi1+i+k) != *(midi2+j+k))
        {
          same = false;
        }
      }
      // Check if we already scored the passage 
      for(auto p : notes)
      {
        if(p == tmp)
        {
          repeated = true;
        }
      }

      if(same && !repeated)
      {
        cpt++;
        notes.push_back(tmp);
      }
    }
  }

  return cpt;
}
int min_globale(int iavant,int javant,int iavantjavant, int dif) {
  int iavnew = iavant +2;
  int jaavnew = javant +2;
  int iavjavnew = iavantjavant + dif;
  if( iavnew < jaavnew) {
    if(iavnew < iavjavnew) {
      return iavnew;
    } else {
      return iavjavnew;
    }
  } else {
    if(jaavnew< iavjavnew) {
      return jaavnew;
    } else {
      return iavjavnew;
    }
  }
}
int max_locale(int iavant,int javant,int iavantjavant, int dif) {
  int iavnew = iavant -2;
  int jaavnew = javant -2;
  int iavjavnew = iavantjavant - dif;
  if( iavnew > jaavnew) {
    if(iavnew > iavjavnew) {
      return iavnew;
    } else {
      return iavjavnew;
    }
  } else {
    if(jaavnew> iavjavnew) {
      return jaavnew;
    } else {
      return iavjavnew;
    }
  }
}


float traceback_globale(int *midi1, int midiSize1, int *midi2, int midiSize2,bool locale) {
  int M[midiSize1+1][midiSize2+1];
  //initialisation

  if(locale) {
    for(int i = 0; i<midiSize2+1;i++) {
      for(int j = 0; j<midiSize1+1;j++) {
        M[j][i] =0;
      }
    } 
  } else {
    for(int i = 0; i<midiSize1+1;i++) {
      M[0][i] = i * 2;
    }
    for(int i = 0; i<midiSize2+1;i++) {
        M[i][0] = i * 2;
    }
  }
  
    
  int match = 1;
  for(int i = 1;i<midiSize1+1; i++) {
    for(int j = 1;j<midiSize2+1; j++) {
      if(midi1[i] == midi2[j]) {
        match = 0;
      }
      
      if(locale) {
        M[i][j] =  max_locale(M[i-1][j],M[i][j-1],M[i-1][j-1], match);
      }
      else {
        M[i][j] =  min_globale(M[i-1][j],M[i][j-1],M[i-1][j-1], match);
      }
    }
  }
  for(int i = 0;i<midiSize1+1; i++) {
    for(int j = 0;j<midiSize2+1; j++) {
      cout<<M[i][j]<<" ";
    }
    cout <<endl;
  }

  return M[midiSize1][midiSize2];
}




float computeScore (int *midi1, int midiSize1, int *midi2, int midiSize2)
{

 cout << " -----" << endl;
 for (int i = 0; i< midiSize1; i++)
    cout << midi1[i] << " ";
  cout << endl;
  
  for (int i = 0; i< midiSize2; i++)
    cout << midi2[i] << " ";
  cout << endl;

  //float x =traceback_globale(midi1, midiSize1, midi2, midiSize2);
  //return Ngrams (5 , midi1, midiSize1,midi2, midiSize2);
  return traceback_globale(midi1, midiSize1, midi2, midiSize2,true);
}



float compare (int *midi1, int midiSize1, string input2)
{
  /* read MIDI */
  int midi2[MAX_SIZE];
  int midiSize2 = 0;
  
  /* Process */
  read_midi(input2.data(), midi2, &midiSize2);

  /* compute similarity score */
  float D = 0.0;

  D =  computeScore (midi1, midiSize1, midi2, midiSize2);
    
  return D;
}

/* ------------------------------------------ */
/* MIDI */

int midi_compare(int* midi1, int midiSize1, string input2)
{
  string total("");

  struct dirent **eps;
  int n=0;
  float score = 0.0;

  n = scandir (input2.data(), &eps, file_select, alphasort);
  
  if (n >= 0)
    {
      /* Directory Processing */
      
      /* Verify directory name*/
      if (input2[input2.size()-1]!= '/')
	  input2+="/";
      
      int cnt=0;

      cout << endl;
      
      
      for (cnt = 0; cnt < n; cnt++)
       {
	 score = compare(midi1, midiSize1, input2+string(eps[cnt]->d_name));
	 
	 cout << "    -" << string(string(eps[cnt]->d_name)) << " : " << score << endl;
	 
	 free(eps[cnt]);
       }
     
      free(eps);
    }
  else if (access(input2.c_str(), R_OK | F_OK) == 0)
    {
      /* File Processing */
      score = compare(midi1, midiSize1, input2);
      cout <<  endl << "    -" << input2 << " : " << score << endl;
    }
  else 
    {
      cerr << endl << "Input 2 : File or Directory name error" << endl;
    }

  return EXIT_SUCCESS;
}
  

int main(int argc, char* argv[])
{
  if (argc<=2)
    {
      cerr << "Usage : ./compare <input midi dir1> <input midi dir2>"<<endl;
      return 1;
    }

  struct dirent **eps;
  int n=0;
  
  string input1=string(argv[1]);
  
  n = scandir (input1.data(), &eps, file_select, alphasort);

  if (n >= 0)
    {
      /* Directory Processing */
      
      /* Verify directory name*/
      if (input1[input1.size()-1]!= '/')
	  input1+="/";
      
      cerr << "Query file processing " << endl;
      
      int cnt=0;
      
      for (cnt = 0; cnt < n; cnt++)
       {
	 /* read MIDI */
	 int midi1[MAX_SIZE];
	 int midiSize1 = 0;
  
	 /* Process */
	 read_midi(input1+string(eps[cnt]->d_name), midi1, &midiSize1);

	 string s = input1;
	 cout << s << ": ";
	 midi_compare(midi1, midiSize1, string(argv[2]));


	 free(eps[cnt]);
       }
     
      free(eps);
    }
  else if (access(input1.c_str(), R_OK | F_OK) == 0)
    {
      /* File Processing */
	 /* read MIDI */
	 int midi1[MAX_SIZE];
	 int midiSize1 = 0;
  
	 /* Process */
	 read_midi(input1, midi1, &midiSize1);

	 string s = input1;
	 cout << s << ": ";
	 midi_compare(midi1, midiSize1, string(argv[2]));

    }
  else 
    {
      cerr << endl << "Input 2 : File or Directory name error" << endl;
    }

  return EXIT_SUCCESS;
}